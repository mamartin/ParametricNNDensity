import math, sys

sys.path.append("../")

from TensorFlowAnalysis import *

# Masses of the final state particles
md  = 1.96834
mpi = 0.13957
mk  = 0.49368

# Masses and widths of the resonances
mkstar = 0.892
wkstar = 0.050
mrho = 0.770
wrho = 0.150

# Eta acceptance
etamin = 2. 
etamax = 5.

# Declare phase spaces
dlzPhsp = DalitzPhaseSpace( mk, mpi, mpi, md )   # conventional Dalitz plot for Ds -> K pi pi 
sqDlzPhsp = RectangularPhaseSpace( ((0., 1.), (0., 1.)) )     # square Dalitz plot
mPhsp = RectangularPhaseSpace( ((1.97-0.2, 1.97+0.2), ) )     # m(Ds) range
observablesPhaseSpace = CombinedPhaseSpace(sqDlzPhsp, mPhsp)  # Combination of square DP and m(Ds) as the observables phase space

expPhaseSpace = VetoPhaseSpace(observablesPhaseSpace, 2, (1.97-0.05, 1.97+0.05) )  # The phase space of the "experimental" data sample
                                                                                   # with vetoed "signal" region
#expPhaseSpace = VetoPhaseSpace(observablesPhaseSpace, 2, (1.97-0.2, 1.97+0.05) )  # The phase space of the "experimental" data sample
#                                                                                   # with vetoed "signal" region

randomArraySize = 12 # Number of rows for auxiliary random tensor to be used for toy MC

observablesTitles = [ "m'", "#theta'", "m(K^{+}#pi^{#font[122]{-}}#pi^{+}) (GeV)" ]

observablesData = [ "mprime", "thetaprime", "md" ]

observablesToys = [ "mprime", "thetaprime", "md" ]

generatedVariables = [ "mprime", "thetaprime", "md", "m2kpi", "m2pipi" ]

parametersList = [
    ("kmeanpt",  "Mean p_{T}(K) (GeV)",   (0.2, 1.),  None), 
    ("pimeanpt", "Mean p_{T}(#pi) (GeV)", (0.2, 1.),  None), 
    ("ptcut",    "Track p_{T} cut (GeV)", (0.1, 0.5), None), 
    ("pcut",     "Track p cut (GeV)",     (1.0, 4.0), -6.), 
    ("kstarmeanpt", "Mean p_{T}(K^{*}) (GeV)",   (0.5, 3.),  None), 
    ("rhomeanpt",   "Mean p_{T}(#rho) (GeV)",    (0.5, 3.),  None), 
    ("kstarfrac",   "K^{*} fraction",   (0., 0.3),  None), 
    ("rhofrac",     "#rho fraction",    (0., 0.3),  None), 
]

# "True" values of the model parameters to be used for generation of the test sample
trueCuts = [ Const(0.3), Const(0.6), Const(0.3), Const(3.0), Const(2.0), Const(2.0), Const(0.10), Const(0.20) ]

bounds = { i[0] : (i[2], i[3]) for i in parametersList }

def UniformRandom(rnd, x1, x2) : 
  """
    Uniform random numbers from x1 to x2
  """
  return x1 + rnd*(x2-x1)

def NormalRandom(rnd1, rnd2) : 
  """
    Normal distribution from two random numbers
  """
  return Sqrt(-2.*Log(rnd1))*Cos(2.*math.pi*rnd2)

def BreitWignerRandom(rnd, mean, gamma) : 
  """
    Random Breit-Wigner distribution with specified mean and width
  """
  rval = 2.*rnd - 1
  displ = 0.5*gamma*Tan(rval*math.pi/2.)
  return mean + displ

def GenerateExp(rnd, x1, x2, alpha = None) : 
  """
    Exponential random distribution with constant "alpha", 
    limited to the range x1 to x2
  """
  if isinstance(x1, float) : x1 = Const(x1)
  if isinstance(x2, float) : x2 = Const(x2)
  if alpha is None or alpha == 0 : 
    return UniformRandom(rnd, x1, x2)
  else : 
    if isinstance(alpha, float) : alpha = Const(alpha)
    xi1 = Exp(-x1/alpha)
    xi2 = Exp(-x2/alpha)
    ximin = Min(xi1, xi2)
    ximax = Max(xi1, xi2)
    return Abs(alpha*Log(UniformRandom(rnd, ximin, ximax) ) )

def GeneratePt(rnd, mean, cut1, cut2) : 
  """
    Generate Pt distribution, with mean "mean", and miminum Pt "cut"
  """
  return GenerateExp(rnd, cut1, cut2, mean)

def GenerateEta(rnd) : 
  """
    Generate pseudorapidity, uniform from 2 to 5.
  """
  return UniformRandom(rnd, 2., 5.)    # Eta, uniform in (2., 5.)

def GeneratePhi(rnd) : 
  """
    Generate phi, uniform in 0, 2pi
  """
  return UniformRandom(rnd, 0., 2.*math.pi) # Phi, uniform in (0, 2pi)

def MomentumResolution(p) :
  """
    Relative momentum resolution as a function of momentum (here = 0.5%, constant)
  """
  return 0.005

def MomentumScale(dm, moms) :
  """
    Function to calculate the momentum scale vactor for kinematic fit
      dm   : invariant mass shift from the desired value
      moms : list of 4-momenta of the final state particles
  """
  psum = sum(moms)  # sum of 4-momenta
  pvecsum = SpatialComponents(psum)
  esum = TimeComponent(psum)
  dedd = Const(0.)
  pdpdd = Const(0.)
  for mom in moms : 
    pm   = P(mom)  # Absolute momentum
    em   = TimeComponent(mom) # Energy
    pvec = SpatialComponents(mom) # 3-momentum
    s    = MomentumResolution(pm)
    dedd += s*pm**2/em
    pdpdd += ScalarProduct(pvecsum, pvec)*s
  return -dm/(2.*esum*dedd - 2.*pdpdd)

def KinematicFit(mfit, moms) : 
  """
    Kinematic fit to a fixed invariant mass for a multibody decay. 
    Returns the fitted mass and the list of final state 4-momenta. 
  """
  mcorr = Mass(sum(moms))
  for l in range(3) :
    dm2 = mcorr**2-mfit**2
    delta = MomentumScale(dm2, moms)
    moms2 = []
    for mom in moms :
      m2 = Mass(mom)**2
      momvec = SpatialComponents(mom)*Scalar(1+delta*MomentumResolution(P(mom)))
      mom2 = LorentzVector(
        momvec, 
        Sqrt(m2 + Norm(momvec)**2)
      )
      moms2 += [ mom2 ]
    moms = moms2
    mcorr = Mass(sum(moms))
  return mcorr, moms

def Generate4Momenta(rnd, meanpt, ptcut, m) : 
  """
    Generate random 4-momenta according to specified mean Pt, minimum Pt, and mass of the particle, flat in eta and phi
  """
  pt  = GeneratePt(rnd[:,0], meanpt, ptcut, Const(200.))  # Pt in GeV
  eta = GenerateEta(rnd[:,1])         # Eta
  phi = GeneratePhi(rnd[:,2])         # Phi

  theta = 2.*Atan(Exp(-eta))
  p  = pt/Sin(theta)     # Full momentum
  e  = Sqrt(p**2 + m**2) # Energy
  px = p*Sin(theta)*Sin(phi)
  py = p*Sin(theta)*Cos(phi)
  pz = p*Cos(theta)
  return LorentzVector(Vector(px, py, pz), e)

def GenerateRotationAndBoost(moms, minit, meanpt, ptcut, rnd) : 
  """
    Generate 4-momenta of final state products boosted to lab frame and randomly rotated
      moms   - initial particle momenta (in the rest frame)
      minit  - mass of the initial particle
      meanpt - mean Pt of the initial particle
      ptcut  - miminum Pt of the initial particle
      rnd    - Auxiliary random tensor with 6 rows
  """

  pt  = GeneratePt(rnd[:,0], meanpt, ptcut, 200.)  # Pt in GeV
  eta = GenerateEta(rnd[:,1])          # Eta
  phi = GeneratePhi(rnd[:,2])          # Phi

  theta = 2.*Atan(Exp(-eta))     # Theta angle
  p  = pt/Sin(theta)             # Full momentum
  e  = Sqrt(p**2 + minit**2)     # Energy 

  px = p*Sin(theta)*Sin(phi)     # 3-momentum of initial particle
  py = p*Sin(theta)*Cos(phi)
  pz = p*Cos(theta)

  p4 = LorentzVector(Vector(px, py, pz), e)  # 4-momentum of initial particle

  rotphi   = UniformRandom(rnd[:,3], 0., 2*Pi())
  rotpsi   = UniformRandom(rnd[:,4], 0., 2*Pi())
  rottheta = Acos(UniformRandom(rnd[:,5], -1, 1.))

  moms2 = []
  for m in moms : 
    m1 = RotateLorentzVector(m, rotphi, rottheta, rotpsi)
    moms2 += [ BoostFromRest(m1, p4) ]

  return moms2

def GenerateCombinatorial(cuts, rnd) : 
  """
    Generate random combinations of three tracks
  """
  meankpt  = cuts[0]
  meanpipt = cuts[1]
  ptcut    = cuts[2]

  p4pi1 = Generate4Momenta(rnd[:,0:3], meanpipt, ptcut, Const(mpi) )
  p4pi2 = Generate4Momenta(rnd[:,3:6], meanpipt, ptcut, Const(mpi) )
  p4k   = Generate4Momenta(rnd[:,6:9], meankpt,  ptcut, Const(mk) )

  return p4k, p4pi1, p4pi2

def GenerateKstar(cuts, rnd) : 
  """
    Generate random combinations of Kstar and a pion track
  """
  meankstarpt = cuts[4]
  meanpipt  = cuts[1]
  ptcut     = cuts[2]

  mkstargen  = BreitWignerRandom(rnd[:,0], mkstar, wkstar)

  ones = Ones(rnd[:,0])
  p = TwoBodyMomentum( mkstargen, mk*ones, mpi*ones )
  zeros = Zeros(p)
  mom = [ LorentzVector( Vector(p, zeros, zeros), Sqrt(p**2 + mk**2)), 
          LorentzVector(-Vector(p, zeros, zeros), Sqrt(p**2 + mpi**2)) ]

  mom   = GenerateRotationAndBoost(mom, mkstargen, meankstarpt, ptcut, rnd[:,2:8] )
  p4k = mom[0]
  p4pi1 = mom[1]
  p4pi2 = Generate4Momenta(rnd[:,8:11], meanpipt, ptcut, Const(mpi) )

  return p4k, p4pi1, p4pi2

def GenerateRho(cuts, rnd) : 
  """
    Generate random combinations of rho -> pi pi  and a kaon track
  """
  meanrhopt = cuts[5]
  meankpt   = cuts[0]
  ptcut     = cuts[2]

  mrhogen  = BreitWignerRandom(rnd[:,0], mrho, wrho)

  ones = Ones(rnd[:,0])
  p = TwoBodyMomentum( mrhogen, mpi*ones, mpi*ones )
  zeros = Zeros(p)
  mom = [ LorentzVector( Vector(p, zeros, zeros), Sqrt(p**2 + mpi**2)), 
          LorentzVector(-Vector(p, zeros, zeros), Sqrt(p**2 + mpi**2)) ]

  mom   = GenerateRotationAndBoost(mom, mrhogen, meanrhopt, ptcut, rnd[:,2:8] )
  p4k   = Generate4Momenta(rnd[:,8:11], meankpt, ptcut, Const(mk) )
  p4pi1 = mom[0]
  p4pi2 = mom[1]

  return p4k, p4pi1, p4pi2

def GenerateSelection(cuts, rnd, constant_cuts = False) : 
  """
    Call generation of fully combinatorial or combinatorial with intermediate K* or rho resonances with specified fractions. 
    Apply cuts to the final state particles and fill in output arrays. 
  """
  meankpt     = cuts[0]
  meanpipt    = cuts[1]
  ptcut       = cuts[2]
  pcut        = cuts[3]
  meankstarpt = cuts[4]
  meanrhopt   = cuts[5]
  kstarfrac   = cuts[6]
  rhofrac     = cuts[7]

  p4k_1, p4pi1_1, p4pi2_1 = GenerateCombinatorial(cuts, rnd)
  p4k_2, p4pi1_2, p4pi2_2 = GenerateKstar(cuts, rnd)
  p4k_3, p4pi1_3, p4pi2_3 = GenerateRho(cuts, rnd)

  thr1 = 1.-kstarfrac-rhofrac
  thr2 = 1.-rhofrac

  p4k   = tf.where(rnd[:,11]<thr1, p4k_1, tf.where(rnd[:,11]<thr2, p4k_2, p4k_3))
  p4pi1 = tf.where(rnd[:,11]<thr1, p4pi1_1, tf.where(rnd[:,11]<thr2, p4pi1_2, p4pi1_3))
  p4pi2 = tf.where(rnd[:,11]<thr1, p4pi2_1, tf.where(rnd[:,11]<thr2, p4pi2_2, p4pi2_3))

  mb = Mass(p4k + p4pi1 + p4pi2)
  mfit, moms = KinematicFit(Const(md), [ p4k, p4pi1, p4pi2 ] )

  sel = tf.greater( P(moms[0]), pcut )
  sel = tf.logical_and(sel, tf.greater( P(moms[1]), pcut ) )
  sel = tf.logical_and(sel, tf.greater( P(moms[2]), pcut ) )
  sel = tf.logical_and(sel, tf.greater( Pt(moms[0]), ptcut ) )
  sel = tf.logical_and(sel, tf.greater( Pt(moms[1]), ptcut ) )
  sel = tf.logical_and(sel, tf.greater( Pt(moms[2]), ptcut ) )

  m2kpi  = Mass(moms[0] + moms[1])**2
  m2pipi = Mass(moms[1] + moms[2])**2

  sample = dlzPhsp.FromVectors(m2kpi, m2pipi)
  mprime = dlzPhsp.MPrimeBC(sample)
  thetaprime = dlzPhsp.ThetaPrimeBC(sample)

  sel = tf.logical_and(sel, observablesPhaseSpace.Inside( tf.stack( [mprime, thetaprime, mb] , axis = 1) ) )

  observables = []
  outlist = [ mprime, thetaprime, mb, m2kpi, m2pipi ]

  if not constant_cuts : outlist += [ meankpt, meanpipt, ptcut, pcut, meankstarpt, meanrhopt, kstarfrac, rhofrac ]
  for i in outlist : 
    observables += [ tf.boolean_mask(i, sel) ]

  return observables

def GenerateCandidatesAndCuts(rnd) : 
  """
    Generate random cuts and call toy MC generation with those cuts to train parametric background model. 
  """

  cuts = []
  for i in range(len(bounds)) : 
    par = parametersList[i][0]
    alpha = parametersList[i][3]
    cuts += [ GenerateExp(rnd[:,i], bounds[par][0][0], bounds[par][0][1], alpha) ]

  return GenerateSelection(cuts, rnd[:,len(bounds):])
