import sys, os

sys.path.append("../")

from TensorFlowAnalysis import *

SetSinglePrecision()

from nn import create_weights_biases, init_weights_biases, multilayer_perceptron, multidim_display

#os.environ["CUDA_VISIBLE_DEVICES"] = ""

from ROOT import gROOT, gStyle
from root_numpy import fill_hist, rec2array, root2array, array2root

from DistributionModel import expPhaseSpace, observablesToys, observablesTitles

gROOT.ProcessLine(".x ../lhcbstyle2.C")
gStyle.SetPalette(56)  # 107

variables = observablesToys
titles = observablesTitles

bounds = expPhaseSpace.Bounds()

phsp = expPhaseSpace

def estimate_density( 
  phsp, 
  calibfile, 
  variables, 
  bounds, 
  titles, 
  weight = None, 
  treename = "tree", 
  learning_rate = 0.001, 
  training_epochs = 100000, 
  norm_size = 1000000, 
  print_step = 50, 
  display_step = 500, 
  weight_penalty = 1., 
  n_hidden  = [ 32, 8 ], 
  path = "./", 
  initfile = "init.npy", 
  outfile = "train", 
  selection = "", 
  seed = 1, 
  mass_penalty_fraction = 10., 
  plots = None
  ) : 

  sample = root2array(path + calibfile, treename = treename, branches = variables, selection = selection)
  data_sample = rec2array(sample, variables)

  n_input = len(variables)

  data_ph = phsp.data_placeholder
  norm_ph = phsp.norm_placeholder

  try : 
    init_w = np.load(initfile)
  except : 
    init_w = None

  if isinstance(init_w, np.ndarray) : 
    print "Loading saved weights"
    (weights1, biases1) = init_weights_biases(init_w)
#    (weights2, biases2) = init_weights_biases(init_w[2:4])
#    alpha1 = tf.Variable(init_w[4], dtype = FPType())
#    alpha2 = tf.Variable(init_w[5], dtype = FPType())
  else : 
    print "Creating random weights"
    (weights1, biases1) = create_weights_biases(n_input, n_hidden)
#    (weights2, biases2) = create_weights_biases(n_input-1, n_hidden)
#    alpha1 = tf.Variable(0., dtype = FPType())
#    alpha2 = tf.Variable(0., dtype = FPType())

  def model(x) : 
#    mlp1 = multilayer_perceptron(x[:,0:2], weights1, biases1)
#    mlp2 = multilayer_perceptron(x[:,0:2], weights2, biases2)
#    m = x[:,2]
#    return (mlp1*Exp(-alpha1*m) + mlp2*Exp(-alpha2*m))**2
    return multilayer_perceptron(x, weights1, biases1)

  def regularisation(weights) : 
    penalty = 0.
    for w in weights : 
        penalty += tf.reduce_sum(tf.square(w))
    return penalty + mass_penalty_fraction*tf.reduce_sum(tf.square(weights[0][2,:]))

  np.random.seed(seed)
  tf.set_random_seed(seed)

  data_model = model(data_ph)
  norm_model = model(norm_ph)

  # Define loss and optimizer
  nll = UnbinnedNLL( data_model, Integral( norm_model ) ) + regularisation(weights1)*weight_penalty

  optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate)
  train_op = optimizer.minimize(nll)

  # Initializing the variables
  init = tf.global_variables_initializer()

  with tf.Session() as sess :
    sess.run(init)

    data_sample = sess.run(phsp.Filter(data_sample))

    if display_step != 0 : 
      display = multidim_display(data_sample, titles, bounds, plots = plots, bins1d = 40, bins2d = 40 )

    norm_sample = sess.run( phsp.UniformSample( norm_size ) )
    print "Normalisation sample size = ", len(norm_sample)
    print norm_sample
    print "Data sample size = ", len(data_sample)
    print data_sample

    # Training cycle
    best_cost = 1e10
    for epoch in range(training_epochs):

        if display_step != 0 and (epoch % display_step == 0) : 

            norm_pdf = sess.run(norm_model, feed_dict = { norm_ph : norm_sample } )
            display.draw(norm_sample, norm_pdf, outfile + ".pdf")

        # Run optimization op (backprop) and cost op (to get loss value)
        _, c = sess.run([train_op, nll], feed_dict={data_ph: data_sample, norm_ph: norm_sample })
        # Display logs per epoch step
        if epoch % print_step == 0 : 
            s = "Epoch %d, cost %.9f" % (epoch+1, c)
            print s
            if c < best_cost : 
              best_cost = c
              np.save(outfile, sess.run( [ weights1, biases1 ] ))
              f = open(outfile + ".txt", "w")
              f.write(s + "\n")
              f.close()

    print("Optimization Finished!")

plots = [ 0, 1, 2, (0, 1), (1, 2), (0, 2) ] 

estimate_density(
  phsp = phsp, 
  variables = variables, 
  bounds = bounds, 
  titles = titles, 
  treename = "tree", 
  learning_rate = 0.001, 
  training_epochs = 10000, 
  print_step = 50, 
  display_step = 200, 
  weight_penalty = 1., 
  n_hidden  = [ 32, 64, 32, 8 ], 
  norm_size = 500000, 
  path = "./", 
  initfile = "init_3d.npy", 
  calibfile = "test_tuple.root", 
  outfile = "train_3d", 
  selection = "", 
  mass_penalty_fraction = 10., 
  seed = 0, 
  plots = plots 
)
