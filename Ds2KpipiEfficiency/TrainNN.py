import sys, os

sys.path.append("../")

from TensorFlowAnalysis import *
from nn import estimate_density

#os.environ["CUDA_VISIBLE_DEVICES"] = ""

from ROOT import gROOT, gStyle

from DistributionModel import parametersList, observablesPhaseSpace, observablesToys, observablesTitles

gROOT.ProcessLine(".x ../lhcbstyle2.C")
#gStyle.SetPadRightMargin(0.15)
#gStyle.SetPadBottomMargin(0.20)
#gStyle.SetPadLeftMargin(0.20)
gStyle.SetPalette(56)
#gStyle.SetPalette(107)

SetSinglePrecision()

variables = observablesToys + [ i[0] for i in parametersList ]
titles = observablesTitles + [ i[1] for i in parametersList ]
parametersBounds = [ i[2] for i in parametersList ]

paramatersPhaseSpace = RectangularPhaseSpace( parametersBounds )
bounds = observablesPhaseSpace.Bounds() + parametersBounds

phsp = CombinedPhaseSpace( observablesPhaseSpace, paramatersPhaseSpace )

plots = range(7) + [ (0, i) for i in range(1, 7) ] + [ (1, i) for i in range(2, 7) ]

estimate_density(
  phsp = phsp, 
  variables = variables, 
  bounds = bounds, 
  titles = titles, 
  treename = "tree", 
  learning_rate = 0.001, 
  training_epochs = 30000, 
  print_step = 50, 
  display_step = 200, 
  weight_penalty = 0.5, 
  n_hidden  = [ 32, 64, 32, 8 ], 
  norm_size = 500000, 
  path = "./", 
  initfile = "init.npy", 
  calibfile = "toy_tuple.root", 
  outfile = "eff_train", 
  selection = "", 
  seed = 1, 
  plots = plots)
