import tensorflow as tf
import sys, os

sys.path.append("../")

from TensorFlowAnalysis import *

import numpy as np
from root_numpy import root2array, rec2array, array2root
from rootpy.plotting import Hist2D, Hist1D

from DistributionModel import observablesData, observablesTitles, observablesPhaseSpace
from nn import init_fixed_weights_biases, multilayer_perceptron, multidim_display
from ROOT import gROOT, gStyle, TCanvas

os.environ["CUDA_VISIBLE_DEVICES"] = ""   # Do not use GPU

gROOT.ProcessLine(".x ../lhcbstyle.C")
gStyle.SetPalette(56)
gStyle.SetPadRightMargin(0.20)
gStyle.SetPadLeftMargin(0.16)
gStyle.SetTitleOffset(0.9, "Z")
gStyle.SetTitleOffset(0.9, "Y")

norm_size = 1000000    # size of the normalisation sample (random uniform)
initfile = "eff_train_2d.npy"           # file with the trained parameters of the NN

init_w = np.load(initfile)       # Load NN parameters 

# TF placeholders for data and normalisation sample to be used for the likelihood graph
norm_ph = observablesPhaseSpace.norm_placeholder

# Initialise NN weights and biases from the loaded file
(weights, biases) = init_fixed_weights_biases(init_w)

ndim = observablesPhaseSpace.Dimensionality()
observablesBounds = observablesPhaseSpace.Bounds()

# Density model as a multilayer perceptron
def model(x) : 
  return multilayer_perceptron(x, weights, biases)

# Create graphs for model over data and normalisation samples
norm_model = model(norm_ph)

# Initializing the variables
init = tf.global_variables_initializer()

with tf.Session() as sess :
    sess.run(init)

    norm_sample = sess.run( observablesPhaseSpace.UniformSample( norm_size ) )
    majorant = EstimateMaximum(sess, norm_model, norm_ph, norm_sample)*1.2
    fit_sample = RunToyMC(sess, norm_model, norm_ph, observablesPhaseSpace, 1000000, majorant, chunk = 1000000)

    struct_sample = np.core.records.fromarrays(fit_sample.transpose(), names = observablesData)
    array2root(struct_sample, "eff_fit_result_2d.root", mode = "recreate")

    norm_sample = sess.run( observablesPhaseSpace.RectangularGridSample( ( 200, 200 ) ) )
    print norm_sample

    norm_pdf = sess.run(norm_model, feed_dict = { norm_ph : norm_sample } )

    h = Hist2D(200, 0., 1., 200, 0., 1.)
    h.fill_array(norm_sample[:,(0,1)], norm_pdf)
    h.Scale(200.*200./h.GetSumOfWeights() )
    h.SetMinimum(0.)

    c = TCanvas("c2", "", 600, 500)
    h.Draw("zcol")
    h.GetXaxis().SetTitle("m'")
    h.GetYaxis().SetTitle("#theta'")
    h.GetZaxis().SetTitle("#varepsilon(m',#theta')")
    c.Print("eff_fit_sqdalitz_2d.pdf")
