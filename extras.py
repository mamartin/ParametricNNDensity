from root_numpy import array2tree, root2array, rec2array
import rootpy.ROOT as ROOT
import numpy as np

def array2dataset(arr, variables, name = 'dataset') : 
  tree = array2tree(arr)
  varset = ROOT.RooArgSet(name + "_argset")
  for var in variables : 
    varset.add(var)
  ds = ROOT.RooDataSet(name, name, tree, varset)
  return ds

def split_array(arr, frac = 0.5) : 
  l = arr.shape[0]
  m = int(round(frac*l))
  np.random.shuffle(arr)
  return (arr[:m,:], arr[m:,:])

def load_array(filename, treename, variables = None, selection = None, transform = None) : 
  print 'Loading array from file ', filename
  array = root2array(filename, treename, branches = variables, selection = selection )
  if transform : 
    for v,func in transform.iteritems() :
      print 'Transforming variable ', v
      array[v] = func(array[v])
  return array

def convert_array(rec, fields = None, transforms = None) : 
  array = rec2array(rec, fields)
  print 'Extracting variables ',str(fields)," from array"
  if transforms : 
    for v,func in transforms.iteritems() :
      i = fields.index(v)
      print 'Transforming variable ', v, ' index ', i
      if i : 
        array[:,i] = func(array[:,i])
  return array

def plot_comparison(arrays, variables, ranges, vartitles, size, splits, colors, options, canvasname, weights = None) : 
  import rootpy.ROOT as ROOT
  from root_numpy import fill_hist

  c = ROOT.TCanvas(canvasname.replace("/","_"),"",size[0],size[1])
  c.Divide(splits[0],splits[1])

  hists = []

  for n,v in enumerate(variables) : 

    hh = []

    for m,a in enumerate(arrays) : 
      h = ROOT.Hist(50, ranges[n][0], ranges[n][1])
      arr = a[v[m]]
      w = None
      if weights and weights[m] : w = a[weights[m]]
      fill_hist(h, arr, w)
      hh += [ h ]

    maximum = hh[0].GetMaximum()
    for m,h in enumerate(hh[1:]) : 
      h.Scale( hh[0].GetSumOfWeights()/h.GetSumOfWeights() )
      h.SetLineColor(colors[m+1])
      h.SetMarkerColor(colors[m+1])
      h.SetMarkerSize(0.05)
      maximum = max(maximum, h.GetMaximum())

    c.cd(n+1)
    hh[0].SetMaximum(maximum*1.2)
    hh[0].Draw(options[0])
    hh[0].SetLineColor(colors[0])
    hh[0].SetMarkerColor(colors[0])
    hh[0].GetXaxis().SetTitle(vartitles[n])
    for m,h in enumerate(hh[1:]) : 
      h.Draw(options[m+1] + " same")

    hists += [ hh ]
  c.Print(canvasname + ".pdf")
  return (c, hists)
