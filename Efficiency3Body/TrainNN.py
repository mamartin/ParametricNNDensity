import sys, os

sys.path.append("../")

from TensorFlowAnalysis import *
from nn import estimate_density

#os.environ["CUDA_VISIBLE_DEVICES"] = ""

from ROOT import gROOT

from DistributionModel import parametersList, observablesPhaseSpace, observablesToys, observablesTitles

gROOT.ProcessLine(".x ../lhcbstyle2.C")

SetSinglePrecision()

variables = observablesToys + [ i[0] for i in parametersList ]
titles = observablesTitles + [ i[1] for i in parametersList ]
parametersBounds = [ i[2] for i in parametersList ]

paramatersPhaseSpace = RectangularPhaseSpace( parametersBounds )
bounds = observablesPhaseSpace.Bounds() + parametersBounds

phsp = CombinedPhaseSpace( observablesPhaseSpace, paramatersPhaseSpace )

estimate_density(
  phsp = phsp, 
  variables = variables, 
  bounds = bounds, 
  titles = titles, 
  treename = "tree", 
  learning_rate = 0.001, 
  training_epochs = 30000, 
  print_step = 50, 
  display_step = 1000, 
  weight_penalty = 1., 
  n_hidden  = [ 32, 64, 32, 8 ], 
  norm_size = 1000000, 
  path = "./", 
  initfile = "init.npy", 
  calibfile = "toy_tuple.root", 
  outfile = "train", 
  selection = "", 
  seed = 1)
