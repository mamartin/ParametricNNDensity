import tensorflow as tf
from root_numpy import array2root
import sys

sys.path.append("../")

from extras import load_array
from TensorFlowAnalysis import *

option = "rec"
if len(sys.argv)>1 : option = sys.argv[1]

if option == "rec" : 
  particles = ["P0", "P1", "P2", "P3"]
  components = ["PX", "PY", "PZ", "PE"]
else : 
  particles = ["Kminus", "Kplus", "piminus", "piplus"]
  components = ["X", "Y", "Z", "E"]

def varname(p, c) : 
  if option == "rec" : 
    return "D0_%s_%s" % (p, c)
  else : 
    return "%s_TRUEP_%s" % (p, c)

# Create list of variables to read
variables = []
for p in particles :
  for c in components : 
    variables += [ varname(p,c) ]

# Read ntuples to numpy array
if option == "rec" : 
  array = load_array("D2hhhh/rec.root", "Prompt_D02KKPiPi/DecayTree", variables )
else : 
  array = load_array("D2hhhh/gen.root", "MyMCDecayTreeTuple/MCDecayTree", variables )

# Initialise TensorFlow
init = tf.global_variables_initializer()
sess = tf.Session()
sess.run(init)

# Fill 4-momenta from the array
moms = []
for p in particles :
  moms += [
            LorentzVector( Vector( Const(array[varname(p, components[0])]), 
                                   Const(array[varname(p, components[1])]), 
                                   Const(array[varname(p, components[2])]) ), 
                           Const(array[varname(p, components[3])]) ) 
          ]

pk1 = moms[0]
pk2 = moms[1]
ppi1 = moms[2]
ppi2 = moms[3]

# initial particle momentum
p = pk1 + pk2 + ppi1 + ppi2

# Boost all particles to mother's rest frame
pk1  = BoostToRest(pk1,  p)
pk2  = BoostToRest(pk2,  p)
ppi1 = BoostToRest(ppi1, p)
ppi2 = BoostToRest(ppi2, p)

# Calculate 5 phase space variables
mkk     = Mass(pk1+pk2)/1000.
mpipi   = Mass(ppi1+ppi2)/1000.
costhk  = CosHelicityAngle(pk1,  pk2)
costhpi = CosHelicityAngle(ppi1, ppi2)
phi     = Azimuthal4Body(pk1, pk2, ppi1, ppi2)

arrays = sess.run( [mkk, mpipi, costhk, costhpi, phi] )

# Store them to ntuple
struct = [ ("mkk", float), ("mpipi", float), ("costhk", float), ("costhpi", float), ("phi", float) ]
recarray = np.rec.fromarrays( arrays, dtype = struct)
if option == "rec" : 
  array2root(recarray, "D2hhhh/rec_5d.root", mode = "recreate")
else : 
  array2root(recarray, "D2hhhh/gen_5d.root", mode = "recreate")
