import array
import sys
import math
import numpy as np
from root_numpy import array2root

sys.path.append("../")

from TensorFlowAnalysis import *
from DistributionModel import parametersList, observablesToys, observablesPhaseSpace, GenerateCandidates, finalStateMultiplicity

import sys, os

os.environ["CUDA_VISIBLE_DEVICES"] = ""   # Do not use GPU

def main() : 

  nev = 100000
  outfile = "test_tuple.root"

  chunk_size = 100000  # Events will be generated in chunks of this size

  SetDoublePrecision()

  struct  = [ (name, float) for name in observablesToys ]

  init = tf.global_variables_initializer() # Initialise TensorFlow
  sess = tf.Session()
  sess.run(init)

  n = 0   # Current tuple size

  # Placeholders for auxiliary random array and Dalitz plot sample
  rnd_ph    = tf.placeholder(FPType(), shape = (None, None), name = "rnd")

  cuts = [
    Const(0.3), Const(0.6), Const(0.3), Const(2.0)
  ] 

  while(True) : 

    # Auxiliary random array
    rnd = np.stack([ Random(chunk_size) for i in range(3*finalStateMultiplicity) ], axis = 1) 
    arrays = sess.run( GenerateCandidates( cuts, rnd_ph, constant_cuts = True ), feed_dict = { rnd_ph : rnd } )

    # Store everything into ROOT file
    recarray = np.rec.fromarrays( arrays , dtype = struct)
    if n == 0 : mode = "recreate"
    else :      mode = "update"
    array2root(recarray, outfile, mode = mode)

    # Increment counters and check if we are done
    size = len(recarray)
    n += size
    if n > nev : break
    print "Selected size = ", n, " last = ", size

if __name__ == "__main__" : 
  main()
