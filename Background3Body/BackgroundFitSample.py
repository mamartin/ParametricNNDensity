import tensorflow as tf

import sys, os
#sys.path.append("../")
#os.environ["CUDA_VISIBLE_DEVICES"] = ""

from ROOT import TH1F, TH2F, TCanvas, TFile, gStyle, gROOT, TPaveLabel

from TensorFlowAnalysis import *
import numpy as np
from root_numpy import root2array, rec2array
from rootpy.plotting import Hist2D, Hist1D

gROOT.ProcessLine(".x lhcbstyle.C")

# Parameters

titles = "m:mpr:th:ksmpt:pimpt:pcut:ptcut".split(":")
bounds = ( (6., 8.), (0., 1.), (0., 1.), (0.2, 1), (0.2, 1), (1., 5.), (0.1, 0.5) )
sig_bounds = ( (4., 6.), (0., 1.), (0., 1.), (0.2, 1), (0.2, 1), (1., 5.), (0.1, 0.5) )
print_step = 50
norm_size = 1000000
path = "./"
initfile = "train.npy"
calibfile = "sample.root"
outfile = "sample"
selection = "m>6."
sig_selection = "m<6."
seed = 1

SetDoublePrecision()

phsp = RectangularPhaseSpace( bounds[:3] )
sig_phsp = RectangularPhaseSpace( sig_bounds[:3] )

# Network Parameters
n_input = len(bounds)
#n_hidden = [ 16, 16, 16, 16 ]

init_w = np.load(initfile)

# Initialise weights and biases from numpy array
def init_weights_biases( init ) : 
  init_weights = init[0]
  init_biases = init[1]
  print len(init_weights), len(init_biases)
  weights = []
  biases = []
  for i in range(len(init_weights)-1) :
    print init_weights[i].shape, init_biases[i].shape
    weights += [ tf.constant(init_weights[i], dtype = FPType()) ]
    biases  += [ tf.constant(init_biases[i], dtype = FPType()) ]
  print init_weights[-1].shape, init_biases[-1].shape
  weights += [ tf.constant(init_weights[-1], dtype = FPType()) ]
  biases  += [ tf.constant(init_biases[-1], dtype = FPType()) ]
  return (weights, biases)

# Multilayer perceptron with fully connected layers defined by matrices of weights and biases
# Use sigmoid function as activation
def multilayer_perceptron(x, weights, biases):
  layer = x
  for i in range(len(weights)) : 
    layer = tf.nn.sigmoid(tf.add(tf.matmul(layer, weights[i]), biases[i]))
  return layer[:,0]

sample = root2array(path + calibfile, treename = "nt", 
                    branches = titles[:3],   # only 3 first variables are data, the others are constant free parameters
                    selection = selection)
sig_sample = root2array(path + calibfile, treename = "nt", 
                    branches = titles[:3],   # only 3 first variables are data, the others are constant free parameters
                    selection = sig_selection)

data_sample = rec2array(sample, titles[:3])
sig_data_sample = rec2array(sig_sample, titles[:3])

data_ph = phsp.data_placeholder
norm_ph = phsp.norm_placeholder

(weights, biases) = init_weights_biases(init_w)

def model(x, pars) : 
  vec = tf.reshape( tf.concat( [ tf.constant([0., 0., 0. ], dtype = FPType()), pars ], axis = 0 ), [ 1, 3 + len(pars) ] )
  x2 = tf.pad( x, [[0, 0], [0, len(pars)]], 'CONSTANT') + vec
  return multilayer_perceptron(x2, weights, biases)

np.random.seed(seed)
tf.set_random_seed(seed)

pars = [ FitParameter(titles[i], (bounds[i][0]+bounds[i][1])/2., bounds[i][0], bounds[i][1]) for i in range(3, len(titles)) ]

data_model = model(data_ph, pars)
norm_model = model(norm_ph, pars)

# Define loss and optimizer
nll = UnbinnedNLL( data_model, Integral( norm_model ) )

# Initializing the variables
init = tf.global_variables_initializer()

c = TCanvas("c", "c", 600, 800)
c.Divide(2,3, 0.005, 0.005)

h_mpr_th   = Hist2D(25,0.,1.,25,0.,1.)
h_mpr_th_f = Hist2D(25,0.,1.,25,0.,1.)
h_mpr    = Hist1D(50,0.,1.)
h_mpr_f  = Hist1D(50,0.,1.)
h_th     = Hist1D(50,0.,1.)
h_th_f   = Hist1D(50,0.,1.)
h2_mpr    = Hist1D(50,0.,1.)
h2_mpr_f  = Hist1D(50,0.,1.)
h2_th     = Hist1D(50,0.,1.)
h2_th_f   = Hist1D(50,0.,1.)

h_mpr_f.SetLineColor(2)
h_th_f.SetLineColor(2)
h2_mpr_f.SetLineColor(2)
h2_th_f.SetLineColor(2)
h_mpr_f.SetLineWidth(3)
h_th_f.SetLineWidth(3)
h2_mpr_f.SetLineWidth(3)
h2_th_f.SetLineWidth(3)

h_mpr_th.GetXaxis().SetTitle("m'")
h_mpr_th.GetYaxis().SetTitle("#theta'")
h_mpr_th_f.GetXaxis().SetTitle("m'")
h_mpr_th_f.GetYaxis().SetTitle("#theta'")


h_mpr.GetXaxis().SetTitle("m'")
h_mpr.GetYaxis().SetTitle("Entries")
h_th.GetXaxis().SetTitle("#theta'")
h_th.GetYaxis().SetTitle("Entries")

h2_mpr.GetXaxis().SetTitle("m'")
h2_mpr.GetYaxis().SetTitle("Entries")
h2_th.GetXaxis().SetTitle("#theta'")
h2_th.GetYaxis().SetTitle("Entries")

with tf.Session() as sess :
    sess.run(init)

    norm_sample = sess.run( phsp.UniformSample( norm_size ) )
    sig_norm_sample = sess.run( sig_phsp.UniformSample( norm_size ) )

    print "Normalisation sample size = ", len(norm_sample)
    print norm_sample
    print "Data sample size = ", len(data_sample)
    print data_sample

    result = RunMinuit(sess, nll, { data_ph : data_sample, norm_ph : norm_sample }, runHesse = True )
    print result
    WriteFitResults(result, "result.txt")

    print("Optimization Finished!")

    norm_pdf = sess.run(norm_model, feed_dict = { norm_ph : norm_sample } )
    sig_norm_pdf = sess.run(norm_model, feed_dict = { norm_ph : sig_norm_sample } )

    h_mpr_th.fill_array(data_sample[:,1:3])
    h_mpr_th_f.fill_array(norm_sample[:,1:3], norm_pdf)

    h_mpr.fill_array(data_sample[:,1])
    h_mpr_f.fill_array(norm_sample[:,1], norm_pdf)
    h_th.fill_array(data_sample[:,2])
    h_th_f.fill_array(norm_sample[:,2], norm_pdf)

    h2_mpr.fill_array(sig_data_sample[:,1])
    h2_mpr_f.fill_array(sig_norm_sample[:,1], sig_norm_pdf)
    h2_th.fill_array(sig_data_sample[:,2])
    h2_th_f.fill_array(sig_norm_sample[:,2], sig_norm_pdf)

    c.cd(1)
    h_mpr_th.Draw("zcol")
    c.cd(2)
    h_mpr_th_f.Scale( h_mpr_th.GetSumOfWeights()/h_mpr_th_f.GetSumOfWeights() )
    h_mpr_th_f.Draw("zcol")
    c.cd(3)
    h_mpr_f.Scale( h_mpr.GetSumOfWeights()/h_mpr_f.GetSumOfWeights() )
    h_mpr.Draw("e")
    h_mpr_f.Draw("hist l same")
    c.cd(4)
    h_th_f.Scale( h_th.GetSumOfWeights()/h_th_f.GetSumOfWeights() )
    h_th.Draw("e")
    h_th_f.Draw("hist l same")
    c.cd(5)
    h2_mpr_f.Scale( h2_mpr.GetSumOfWeights()/h2_mpr_f.GetSumOfWeights() )
    h2_mpr.Draw("e")
    h2_mpr_f.Draw("hist l same")
    c.cd(6)
    h2_th_f.Scale( h2_th.GetSumOfWeights()/h2_th_f.GetSumOfWeights() )
    h2_th.Draw("e")
    h2_th_f.Draw("hist l same")

    c.Update()
    c.Print("fit.pdf")

