import ROOT 

ROOT.gROOT.ProcessLine(".x ../lhcbstyle.C")

f = ROOT.TFile.Open("sample.root")
nt = f.Get("nt")

h = ROOT.TH1F("h", "", 50, 4., 8.)
h2 = ROOT.TH2F("h2", "", 25, 0., 1., 25, 0., 1.)

nt.Project("h", "m")
nt.Project("h2", "th:mpr")

c = ROOT.TCanvas("c", "", 600, 300)
c.Divide(2,1)

c.cd(1)
h.Draw()
h.GetXaxis().SetTitle("m(B) (GeV)")

l = ROOT.TLine()
l.SetLineColor(2)
l.DrawLine(6., 0., 6., h.GetMaximum() )

t = ROOT.TText()
t.SetTextColor(4)
t.DrawText(4.2, 0.2*h.GetMaximum(), "Signal")
t.DrawText(6.2, 0.7*h.GetMaximum(), "Sideband")

c.cd(2)
h2.Draw("zcol")
h2.GetXaxis().SetTitle("m'")
h2.GetYaxis().SetTitle("#theta'")

c.Update()
c.Print("sample.pdf")
