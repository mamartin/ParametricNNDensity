import tensorflow as tf

import sys, os
#sys.path.append("../")
#os.environ["CUDA_VISIBLE_DEVICES"] = ""

from ROOT import TH1F, TH2F, TCanvas, TFile, gStyle, gROOT, TPaveLabel, TBox

from TensorFlowAnalysis import *
import numpy as np
from root_numpy import root2array, rec2array

gROOT.ProcessLine(".x lhcbstyle2.C")

# Parameters

variables = "m:mpr:th:ksmpt:pimpt:pcut:ptcut".split(":")
bounds = ( (4., 8.), (0., 1.), (0., 1.), (0.2, 1), (0.2, 1), (1., 5.), (0.1, 0.5) )
titles = ("M(K_{S}#pi^{+}#pi^{#font[122]{-}}) (GeV)","m'","#theta'","Mean p_{T}(K_{S}) (GeV)","Mean p_{T}(#pi) (GeV)","Threshold p (GeV)","Threshold p_{T} (GeV)")
learning_rate = 0.001
training_epochs = 100000
print_step = 50
display_step = 500
weight_penalty = 1.
#signal_layers  = [ 32, 32, 32, 16 ]
signal_layers  = [ 16, 32, 32, 8 ]
norm_size = 1000000
path = "./"
initfile = "init.npy"
calibfile = "tuple.root"
outfile = "train"
selection = ""
seed = 1

phsp = RectangularPhaseSpace( bounds )

# Network Parameters
n_input = len(bounds)
#n_hidden = [ 16, 16, 16, 16 ]

try : 
  init_w = np.load(initfile)
except : 
  init_w = None

# Initialise weights and biases from numpy array
def init_weights_biases( init ) : 
  init_weights = init[0]
  init_biases = init[1]
  print len(init_weights), len(init_biases)
  weights = []
  biases = []
  for i in range(len(init_weights)-1) :
    print init_weights[i].shape, init_biases[i].shape
    weights += [ tf.Variable(init_weights[i], dtype = fptype) ]
    biases  += [ tf.Variable(init_biases[i], dtype = fptype) ]
  print init_weights[-1].shape, init_biases[-1].shape
  weights += [ tf.Variable(init_weights[-1], dtype = fptype) ]
  biases  += [ tf.Variable(init_biases[-1], dtype = fptype) ]
  return (weights, biases)

# Create layers weights & biases
def create_weights_biases( n_input, layers) : 
  n_hidden = [ n_input ] + layers
  weights = []
  biases = []
  for i in range(len(n_hidden)-1) : 
    weights += [ tf.Variable(tf.random_normal([n_hidden[i], n_hidden[i+1]], dtype = fptype), dtype = fptype) ] 
    biases  += [ tf.Variable(tf.random_normal([n_hidden[i+1]], dtype = fptype), dtype = fptype) ]
  weights += [ tf.Variable(tf.random_normal([n_hidden[-1], 1], dtype = fptype), dtype = fptype) ]
  biases  += [ tf.Variable(tf.random_normal([1], dtype = fptype), dtype = fptype) ]
  return (weights, biases)

# Multilayer perceptron with fully connected layers defined by matrices of weights and biases
# Use sigmoid function as activation
def multilayer_perceptron(x, weights, biases):
  layer = x
  for i in range(len(weights)) : 
    layer = tf.nn.sigmoid(tf.add(tf.matmul(layer, weights[i]), biases[i]))
  return layer[:,0]

def regularisation(weights) : 
  penalty = 0.
  for w in weights : 
    penalty += tf.reduce_sum(tf.square(w))
  return penalty

sample = root2array(path + calibfile, treename = "nt", 
                    branches = variables, 
                    selection = selection)

data_sample = rec2array(sample, variables)

data_ph = phsp.data_placeholder
norm_ph = phsp.norm_placeholder

if isinstance(init_w, np.ndarray) : 
  print "Loading saved weights"
  (weights, biases) = init_weights_biases(init_w)
else : 
  print "Creating random weights"
  (weights, biases) = create_weights_biases(n_input, signal_layers)

def model(x) : 
  return multilayer_perceptron(x, weights, biases)

np.random.seed(seed)
tf.set_random_seed(seed)

data_model = model(data_ph)
norm_model = model(norm_ph)

# Define loss and optimizer
nll = UnbinnedNLL( data_model, Integral( norm_model ) ) + regularisation(weights)*weight_penalty

optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate)
train_op = optimizer.minimize(nll)

# Initializing the variables
init = tf.global_variables_initializer()

can = TCanvas("c","c", 1000, 850)
can.Divide(n_input,n_input, 0.003, 0.003)

t1 = TPaveLabel(0.68, 0.83, 0.85, 0.93, "Data", "NDC")
t1.SetBorderSize(0)
t1.SetFillColor(0)
t2 = TPaveLabel(0.65, 0.83, 0.85, 0.93, "Fit", "NDC")
t2.SetBorderSize(0)
t2.SetFillColor(0)

b = TBox()
b.SetFillStyle(0)
b.SetLineWidth(1)
b.SetLineColor(2)

hists2d = []
hists1d = []
hists2d_f = []
hists1d_f = []
for i1 in range(n_input) :
  h  = TH1F("h%d" % i1, "",100, bounds[i1][0],  bounds[i1][1])
  hf = TH1F("h%df" % i1,"",100, bounds[i1][0],  bounds[i1][1])
  h.GetXaxis().SetTitle(titles[i1])
  h.GetYaxis().SetTitle("Entries")
  hists1d_f += [ hf ]
  hists1d += [ h ]
  for i2 in range(i1+1, n_input) :
    h  = TH2F("h%d%d" % (i1, i2), "",50, bounds[i1][0],  bounds[i1][1], 50, bounds[i2][0],  bounds[i2][1])
    hf = TH2F("h%d%df" % (i1, i2),"",50, bounds[i1][0],  bounds[i1][1], 50, bounds[i2][0],  bounds[i2][1])
    h.GetXaxis().SetTitle(titles[i1])
    h.GetYaxis().SetTitle(titles[i2])
#    h.GetZaxis().SetTitle("Entries")
    hf.GetXaxis().SetTitle(titles[i1])
    hf.GetYaxis().SetTitle(titles[i2])
#    hf.GetZaxis().SetTitle("Entries")
    hists2d_f += [ hf ]
    hists2d += [ h ]

for i in data_sample :
  n=0
  for n1 in range(n_input) :
    hists1d[n1].Fill(i[n1], 1.)
    for n2 in range(n1+1, n_input) :
      hists2d[n].Fill(i[n1], i[n2], 1.)
      n += 1

for n in range(len(hists2d)) :
#  can.cd(2*n+1)
  can.cd(n+1+(n/n_input)*n_input)
  hists2d[n].SetMinimum(0)
  hists2d[n].Draw("zcol")
  t1.Draw()

with tf.Session() as sess :
    sess.run(init)

    norm_sample = sess.run( phsp.UniformSample( norm_size ) )
    print "Normalisation sample size = ", len(norm_sample)
    print norm_sample
    print "Data sample size = ", len(data_sample)
    print data_sample

    # Training cycle
    best_cost = 1e10
    for epoch in range(training_epochs):

        # Run optimization op (backprop) and cost op (to get loss value)
        _, c = sess.run([train_op, nll], feed_dict={data_ph: data_sample, norm_ph: norm_sample })
        # Display logs per epoch step
        if epoch % print_step == 0 : 
            s = "Epoch %d, cost %.9f" % (epoch+1, c)
            print s
            if c < best_cost : 
              best_cost = c
              np.save(outfile, sess.run( [ weights, biases ] ))
              f = open(outfile + ".txt", "w")
              f.write(s + "\n")
              f.close()

        if display_step != 0 and (epoch % display_step == 0) : 

            norm_pdf = sess.run(norm_model, feed_dict = { norm_ph : norm_sample } )

            for i in hists1d_f : i.Reset()
            for i in hists2d_f : i.Reset()
            for i in zip(norm_sample, norm_pdf) : 
              n = 0
              for n1 in range(n_input) : 
                hists1d_f[n1].Fill(i[0][n1], i[1])
                for n2 in range(n1+1, n_input) : 
                  hists2d_f[n].Fill(i[0][n1], i[0][n2], i[1])
                  n += 1

            for n in range(len(hists2d_f)) : 
#              can.cd(2*n + 2)
              can.cd(n+1+(n/n_input)*n_input+n_input)
              hists2d_f[n].SetMinimum(0)
              hists2d_f[n].Draw("zcol")
              t2.Draw()

            for n in range(n_input) : 
              can.cd(n + 2*len(hists2d)+1)
#              can.cd(n + 2*len(hists2d)+1)
              hists1d_f[n].Scale(hists1d[n].GetSumOfWeights()/hists1d_f[n].GetSumOfWeights())
              hists1d[n].Draw("e")
              hists1d[n].SetMinimum(0.)
              hists1d_f[n].SetMarkerColor(2)
              hists1d_f[n].SetLineColor(2)
              hists1d_f[n].SetMinimum(0.)
              hists1d_f[n].Draw("hist l same")

            can.cd()
            for i in range(n_input) : 
              for j in range(n_input/2) : 
                b.DrawBox(float(i)/float(n_input)+0.002, float(2*j+1)/float(n_input)+0.002, float(i+1)/float(n_input)-0.002, float(2*j+3)/float(n_input)-0.002)

            can.Update()
            can.Print(outfile + ".pdf")

    print("Optimization Finished!")
